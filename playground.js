// associative property of multiplication

const a = 5 * (2 * 3);
console.log("a: ", a);
const b = (5 * 2) * 3;
console.log("b: ", b);
const c = (5 * 3) * 2;
console.log("c: ", c);

function compose(...fns) {
    return fns.reduce(function (f, g) {
        return function (...args) {
            return f(g(...args))
	}
    })
}

const name = "zach overhulser";

function getInitials(name) {
    const names = name.split(" ");
    return names[0][0] + " " + names[1][0];
}

function capitalize(name) {
    return name.toUpperCase();
}

console.log(addDots(name))

// associative because either one can come in front of the other
// and you're left with the same output
const value = compose(capitalize, getInitials)(name);
console.log(value);
