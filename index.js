"use strict";

// Container with somewhere to store value(s)
// Some way of applying a function to value(s)
// Must return value(s) in the same context

// Functor :: { value: Number }
const Just = {
    map (f) {
        return Just.of(f(this.value));
    },
    of (x) { 
        const newFunctor = Object.assign({}, Just);
        newFunctor.value = x;
        return newFunctor; 
    },
    log () {
        console.log(`Just ${this.value}`);
    },
    mjoin () { return this.value },
    chain (f) { return this.map(f).mjoin() }

};

const Maybe = {
    map (f) {
        return isNothing(this.value) ? Nothing.of() : Just.of(f(this.value));
    },
    of (x) { 
        const newFunctor = Object.assign({}, Maybe);
        newFunctor.value = x;
        return newFunctor; 
    },
	mjoin () { return this.value },
	chain (f) { return this.map(f).mjoin() }

};

const Nothing = {
    map (f) {
        return Nothing.of()
    },
    of (x) {
        return Object.assign({}, Nothing)
    },
    log () {
        console.log("Nothing")
    },
    mjoin () { return Nothing.of() },
    chain (f) { return this.map(f).mjoin() }
};

const input = 5;

// transform :: Number => Just Number
const transform = compose(Just.of, add1, add1);
const app = compose(
    log, 
    chain(transform), 
    chain(transform), 
    //mjoin, // replaced by chain
    //map(transform), 
    Maybe.of
); // Maybe Number
const ArrayF = [1, 2, 3, 4, 5];
 
function add1 (n) { return n + 1 }
function map (f) {  return context => context.map(f) }
function mjoin(context) { return context.mjoin() }
// Chain M a -> (a -> M b) -> M b
function chain(f) { return compose(mjoin, map(f)) }
function isNothing(x) { return x === null || x === undefined }

function compose(...funcs) {
    return x => funcs.reduceRight((y, f) => f(y), x)
}
function log(context) {
    context.log !== undefined ? context.log() : console.log(context)
}

app(input);
